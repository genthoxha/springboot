package com.springbootexperiment.demo.rest;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class FunRestController {

    // expose "/" that return "Hello World"
    @GetMapping("/")
    public String sayHello() {
        return "asdfsawefawefsdfasdfasdfdf" + LocalDateTime.now();
    }

    // expose new endpoint
    @GetMapping("/workout")
    public String getDailyWorkout() {
        return "sdfsdf  RUN";
    }
}
