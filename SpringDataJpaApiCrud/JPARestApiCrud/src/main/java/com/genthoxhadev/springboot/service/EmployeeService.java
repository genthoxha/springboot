package com.genthoxhadev.springboot.service;

import com.genthoxhadev.springboot.entity.Employee;

import java.util.List;

public interface EmployeeService {

    public List<Employee> findAll();

    public Employee findById(int id);

    public void save(Employee theEmployee);

    public void deleteById(int theId);

}
