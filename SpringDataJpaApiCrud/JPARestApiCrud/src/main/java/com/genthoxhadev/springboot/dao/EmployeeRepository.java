package com.genthoxhadev.springboot.dao;

import com.genthoxhadev.springboot.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
