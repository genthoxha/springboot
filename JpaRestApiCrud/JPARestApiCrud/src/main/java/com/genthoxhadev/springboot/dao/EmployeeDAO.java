package com.genthoxhadev.springboot.dao;

import com.genthoxhadev.springboot.entity.Employee;

import java.util.List;

public interface EmployeeDAO {

    public List<Employee> findAll();

    public Employee findById(int theId);

    public void save(Employee employee);

    public void deleteById(int id);

}
