package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class FunRestController {

    // inject properties for: coach.name and team.name
    @Value("${coach.name}")
    private String coachName;


    @Value("${caoch.team}")
    private String teamName;

    // expose "/" that return "Hello World"
    @GetMapping("/")
    public String sayHello() {
        return "f" + LocalDateTime.now();
    }

    // expose new endpoint
    @GetMapping("/workout")
    public String getDailyWorkout() {
        return "fff  RUN";
    }

    @GetMapping("/teaminfo")
    public String getTeamInfo() {
        return "Coach name: " + coachName + "\n Team name: " + teamName;
    }


}
